﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Nez;
using Nez.Tiled;

namespace NezSquirrel.Scenes
{
    class PlatformerScene : Scene
    {
        const int playerWidth = 16;
        const int playerHeight = 32;

        private const int designResolutionWidth = 320;
        private const int designResolutionHeight = 180;
        public PlatformerScene() 
        {
                
        }

        public override void initialize()
        {
            addRenderer(new DefaultRenderer());
            // Setup pixel perfect screen
            setDesignResolution(designResolutionWidth, designResolutionHeight, Scene.SceneResolutionPolicy.ShowAllPixelPerfect);
            Screen.setSize(designResolutionWidth * 4, designResolutionHeight * 4);

            var tiledEntity = createEntity("tiled-map-entity");
            var tiledMap = content.Load<TiledMap>("MyMap");
            var spawnObject = tiledMap.getObjectGroup("player").objectWithName("player");
            var tiledMapComponent = tiledEntity.addComponent(new TiledMapComponent(tiledMap, "Foreground"));
            tiledEntity.addComponent(new CameraBounds(new Vector2(tiledMap.tileWidth, tiledMap.tileWidth), new Vector2(tiledMap.tileWidth * (tiledMap.width - 1), tiledMap.tileWidth * (tiledMap.height - 1))));

            var cameraEntity = createEntity("follow-camera");



            var playerEntity = createEntity("player", new Vector2(spawnObject.x,spawnObject.y));
            camera.entity.addComponent(new FollowCamera(playerEntity));
            //player.addComponent(new PrototypeSprite(8, 16)).setColor(Color.Red);
            var boxColliderWidth = playerWidth / 2;
            var boxColliderHeight = playerHeight / 2;
            var b = playerEntity.addComponent(new BoxCollider(-boxColliderWidth / 2f,0f ,boxColliderWidth,boxColliderHeight));
            playerEntity.addComponent(new Player());
            playerEntity.addComponent(new TiledMapMover(tiledMap.getLayer<TiledTileLayer>("Foreground")));
            //playerEntity.addComponent<PlayerController>();
            // TiledMapMover requires a collider

        }
    }
}
