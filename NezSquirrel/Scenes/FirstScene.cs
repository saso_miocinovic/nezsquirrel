﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using Nez;
using Nez.UI;

namespace NezSquirrel.Scenes
{
    class FirstScene : BaseScene
    {
        private Table table;
        public override Table Table
        {
            get { return table; }
            set { table = value; }
        }

        public FirstScene()
        {

        }

        public override void initialize()
        {
            SetupScene();
            Table.add(new Label("Main Menu").setFontScale(5));
            Table.row().setPadTop(20);
            Table.add(new Label("This is main menu for our game").setFontScale(2));
            var playButton =
                Table.add(new TextButton("Next", Skin.createDefaultSkin()))
                    .setFillX()
                    .setMaxHeight(30)
                    .getElement<TextButton>();
            playButton.onClicked += PlayButton_OnClicked;
        }

        private void PlayButton_OnClicked(Button obj)
        {
            Core.startSceneTransition(new TextureWipeTransition(() => new FirstScene())
            {
                //transitionTexture = Core.content.Load<Texture2D>("wink.mgfxo")
            });

        }
    }
}
