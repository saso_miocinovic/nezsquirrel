﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Nez;
using NezSquirrel.Scenes;

namespace NezSquirrel
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Core
    {
        public Game1()
        {
        }

        protected override void Initialize()
        {
            base.Initialize();
            scene = new PlatformerScene();

        }
    }
}
