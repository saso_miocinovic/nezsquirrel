﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Nez;
using Nez.Sprites;
using Nez.Textures;
using Nez.Tiled;

namespace NezSquirrel
{
    class Player : Component, ITriggerListener, IUpdatable
    {
        enum Animations
        {
            Idle,
            Walk,
            Skid,
            Jump,
            Fall
        }

        public float moveSpeed = 150;
        public float gravity = 1000;
        public float jumpHeight = 32 * 3;

        private Sprite<Animations> _animation;
        private TiledMapMover _mover;
        private BoxCollider _collider;
        TiledMapMover.CollisionState _collisionState = new TiledMapMover.CollisionState();
        private Vector2 _velocity;
        private Texture2D _texture;
        private VirtualButton _jumpInput;
        private VirtualIntegerAxis _xAxisInput;

        public override void onAddedToEntity()
        {
            _texture = entity.scene.content.Load<Texture2D>("player-both");
            var subtextures = Subtexture.subtexturesFromAtlas(_texture, 16, 32);
            _collider = entity.getComponent<BoxCollider>();
            _mover = entity.getComponent<TiledMapMover>();
            _animation = entity.addComponent(new Sprite<Animations>(subtextures[0]));

            // Extract animations from atlas
            _animation.addAnimation(Animations.Idle, new SpriteAnimation(new List<Subtexture>()
            {
                subtextures[0]
            }));
            _animation.addAnimation(Animations.Walk, new SpriteAnimation(new List<Subtexture>()
            {
                subtextures[1],
                subtextures[2],
                subtextures[3],
                subtextures[2]
            }));
            _animation.addAnimation(Animations.Skid, new SpriteAnimation(new List<Subtexture>()
            {
                subtextures[4]
            }));
            _animation.addAnimation(Animations.Jump, new SpriteAnimation(new List<Subtexture>()
            {
                subtextures[5]
            }));
            _animation.addAnimation(Animations.Fall, new SpriteAnimation(new List<Subtexture>()
            {
                subtextures[5]
            }));
            SetupInput();
        }

        private void SetupInput()
        {
            _jumpInput = new VirtualButton();
            _jumpInput.nodes.Add(new Nez.VirtualButton.KeyboardKey(Keys.C));
            _jumpInput.nodes.Add(new Nez.VirtualButton.GamePadButton(0, Buttons.A));

            _xAxisInput = new VirtualIntegerAxis();
            _xAxisInput.nodes.Add(new Nez.VirtualAxis.GamePadDpadLeftRight());
            _xAxisInput.nodes.Add(new Nez.VirtualAxis.GamePadLeftStickX());
            _xAxisInput.nodes.Add(new Nez.VirtualAxis.KeyboardKeys(VirtualInput.OverlapBehavior.TakeNewer,Keys.Left,Keys.Right));
        }


        public void onTriggerEnter(Collider other, Collider local)
        {
            throw new NotImplementedException();
        }

        public void onTriggerExit(Collider other, Collider local)
        {
            throw new NotImplementedException();
        }


        void IUpdatable.update()
        {
            var moveDir = new Vector2(_xAxisInput.value, 0);
            var animation = Animations.Idle;

            if (moveDir.X < 0)
            {
                if (_collisionState.below)
                {
                    animation = Animations.Walk;
                }
                _animation.flipX = true;
                _velocity.X = -moveSpeed;
            }
            else if (moveDir.X > 0)
            {
                if (_collisionState.below)
                {
                    animation = Animations.Walk;
                }
                _animation.flipX = false;
                _velocity.X = moveSpeed;
            }

            else
            {
                _velocity.X = 0;
                if (_collisionState.below)
                {
                    animation = Animations.Idle;
                }
            }

            if (_collisionState.below && _jumpInput.isPressed)
            {
                animation = Animations.Jump;
                _velocity.Y = -Mathf.sqrt(2f * jumpHeight * gravity);
            }

            if (_collisionState.below && _velocity.Y > 0)
            {
                animation = Animations.Fall;
            }

            // Apply gravity
            _velocity.Y += gravity * Time.deltaTime;

            // Move
            _mover.move(_velocity * Time.deltaTime, _collider, _collisionState);

            if (_collisionState.below)
            {
                _velocity.Y = 0;
            }

            if (!_animation.isAnimationPlaying(animation))
            {
                _animation.play(animation);
            }
        }
    }
}
