﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Nez;
using Nez.Tiled;

// TODO: https://www.youtube.com/watch?v=bnMDXRb5Y9U Time: 32
namespace NezSquirrel
{
    class PlayerController : Component, IUpdatable
    {
        public float MoveSpeed = 150;
        public float Gravity = 1000;
        public float JumpHeight = 16 * 4;

        private TiledMapMover _mover;
        private BoxCollider _collider;
        private Vector2 _velocity;
        // collisionState -> handles keeping state of the move that it does - it gets filled by mover
        // ^^ Allows that single tiledMapMover can be used by all entities in the scene
        TiledMapMover.CollisionState _collisionState = new TiledMapMover.CollisionState(); 
        /// <summary>
        /// Called when we're added to entity - ie we can access entity components
        /// </summary>
        public override void onAddedToEntity()
        {
            _mover = entity.getComponent<TiledMapMover>();
            _collider = entity.getComponent<BoxCollider>();
        }

        public void update()
        {
            if(Input.isKeyDown(Keys.Right))
            {
                _velocity.X = MoveSpeed;
            }
            else if (Input.isKeyDown(Keys.Left))
            {
                _velocity.X = -MoveSpeed;
            }
           else
            {
                _velocity.X = 0;
            }

            // Jump
            if (Input.isKeyPressed(Keys.C) && _collisionState.below)
            {
                // Velocity that will be just enough to get us to our desired height
                _velocity.Y = -Mathf.sqrt(2 * JumpHeight * Gravity);
            }

            // Gravity (G = acceleration)
            _velocity.Y += Gravity * Time.deltaTime;
            // Velocity * time = distance
            _mover.move(_velocity * Time.deltaTime, _collider, _collisionState);

            // Update velocity after we move
            if (_collisionState.right || _collisionState.left)
            {
                _velocity.X = 0;
            }
            if (_collisionState.above || _collisionState.below)
            {
                _velocity.Y = 0;
            }
        }
    }
}
