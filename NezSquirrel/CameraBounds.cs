﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Nez;

namespace NezSquirrel
{
    class CameraBounds : Component, IUpdatable
    {
        public Vector2 min, max;

        public CameraBounds()
        {
            // Make sure we update last so that the camera is already moved before we evalueate
            // it's position 
            setUpdateOrder(int.MaxValue);
        }

        public CameraBounds(Vector2 min, Vector2 max)
        {
            this.min = min;
            this.max = max;
        }

        public override void onAddedToEntity()
        {
            entity.updateOrder = int.MaxValue;
        }


        public void update()
        {
            var cameraBounds = entity.scene.camera.bounds;

            if (cameraBounds.top < min.Y)
                entity.scene.camera.position += new Vector2(0, min.Y - cameraBounds.top);

            if (cameraBounds.left < min.X)
                entity.scene.camera.position += new Vector2(min.X - cameraBounds.left, 0);

            if (cameraBounds.bottom > max.Y)
                entity.scene.camera.position += new Vector2(0, max.Y - cameraBounds.bottom);

            if (cameraBounds.right > max.X)
                entity.scene.camera.position += new Vector2(max.X - cameraBounds.right, 0);
        }
    }
}
